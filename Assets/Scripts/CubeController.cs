﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {

	public Material mat1;
	public Material mat2;

	// Use this for initialization
	void Start () {
		this.gameObject.GetComponent<Renderer>().material = mat1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Trigger once on mouse pointer enters
	void OnMouseEnter () {
		Debug.Log ("Mouse Enter: " + this.gameObject.name);
		// Change material:
		this.gameObject.GetComponent<Renderer>().material = mat2;
	}

	// Trigger once on mouse pointer exit
	void OnMouseExit () {
		Debug.Log ("Mouse Exit: " + this.gameObject.name);
		// Change material:
		this.gameObject.GetComponent<Renderer>().material = mat1;
	}
}
